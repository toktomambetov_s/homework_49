var figlet = require('figlet');

figlet(process.argv[2], function(err, data) {
   if (err) {
       console.log('Something went wrong...');
       console.log(err);
       return;
   }
   console.log(data);
});